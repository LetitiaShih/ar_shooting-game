﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	[SerializeField] GameObject enemy;
	[SerializeField] Transform enemyParentTransform;
	[SerializeField] float secondBetweenSpawn=3f; 
	[SerializeField] AudioClip spawnEnemySFX;
	void Start () {
		StartCoroutine(SpawnEnemies());
	}
	


	IEnumerator SpawnEnemies()
	{
		while(true)
		{
			GetComponent<AudioSource>().PlayOneShot(spawnEnemySFX);
			
			var newEnemy=Instantiate(enemy,new Vector3(0,0,0),Quaternion.identity);
			newEnemy.transform.parent=enemyParentTransform;
			yield return new WaitForSeconds(secondBetweenSpawn);			
		}

	}

}
