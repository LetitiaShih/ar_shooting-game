﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using System;
using UnityEngine.AI;

public class TapToPlace : MonoBehaviour
{
    public GameObject defenderBase;
    public GameObject placementIndicator;
    public float spawnRange = 1;
    private ARSessionOrigin arOrigin;

    public Type collisionPlane;

    private Pose placementPose;

    private bool placementPoseIsValid = false; 
    private bool hasPlacedDefender = false;
    TowerFactory towerFactory;
    void Start()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>();
        towerFactory = GetComponent<TowerFactory>();
    }
    void Update()
    {
        UpdatePlacementPose(); //TODO: 在放入守衛塔後應該就只要在按下去的時候呼叫就好
        UpdatePlacementIndicator(); //TODO: 在放入守衛塔後就不用執行了
        if(placementPoseIsValid & Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            if(!hasPlacedDefender) 
            {
                PlaceDefender();
            }
            else
            {
                placeCannon();
            }
        }
    }

    private void PlaceDefender()
    {
        Vector3 placementPosition = placementPose.position;
        Vector3 spawnPos = new Vector3(placementPosition.x + UnityEngine.Random.Range(-spawnRange, spawnRange), placementPosition.y, placementPosition.z + UnityEngine.Random.Range(-spawnRange, spawnRange));
        Instantiate(defenderBase, spawnPos, Quaternion.identity);
        hasPlacedDefender = true;
        placementIndicator.SetActive(false);

    }

    private void UpdatePlacementIndicator()
    {
        if(placementPoseIsValid && !hasPlacedDefender)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f,0.5f));
        if(!hasPlacedDefender) 
        {
            //show screenCenter for placing ground
            var hits = new List<ARRaycastHit>();
            arOrigin.Raycast(screenCenter, hits, TrackableType.Planes);
            placementPoseIsValid = hits.Count > 0;
            if(placementPoseIsValid)
            {
                placementPose = hits[0].pose;
                var cameraForward = Camera.current.transform.forward;
                var cameraBwaring = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
                placementPose.rotation = Quaternion.LookRotation(cameraBwaring);
            
            }
        }
        else if(hasPlacedDefender)  
        {
            //update touchPosition for objects
            var objectHits = new List<ARRaycastHit>();
            if(Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                arOrigin.Raycast(touch.position, objectHits, TrackableType.Planes);
                placementPose = objectHits[0].pose;
            }            

        }
    }
    public void placeCannon()
    {
        towerFactory.AddTower(placementPose);
    }

}
