﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderHealth : MonoBehaviour {
	[SerializeField]float maxHp=10;
	[SerializeField]int Damage=1;
	[SerializeField] ParticleSystem deathEffect;
	[SerializeField]float hp;
	[SerializeField] AudioClip playerDamageSFX;


	// Use this for initialization
	void Start () {
		hp=maxHp;
	}
	
	// Update is called once per frame
	private void OnTriggerEnter(Collider other)
	{
		if(hp>1)
		{
			hp=hp-Damage;
			
		}
		else
		{
		var deathEffectSpawnPos=new Vector3(transform.position.x,5, transform.position.z);
		var deathEffectIns=Instantiate(deathEffect,deathEffectSpawnPos,Quaternion.identity);
		deathEffectIns.Play();
		AudioSource.PlayClipAtPoint(playerDamageSFX,Camera.main.transform.position);
		Destroy(deathEffectIns.gameObject,deathEffectIns.main.duration);
		Destroy(gameObject);	
		}
		
	}
	public float GetRatioOfLife()
	{
		return hp/maxHp;
	}
}
