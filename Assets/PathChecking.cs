﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathChecking : MonoBehaviour
{
    
    public GameObject defenderBase;
   public LayerMask Tower;
   float speed = 10;
   Vector3 target;
   bool touchTower = false;
   bool touchDefender = false;
    void Start()
    {
        defenderBase = GameObject.FindGameObjectWithTag("DefenderBase");
        CheckPath();
    }

    // Update is called once per frame
    void Update()
    {
        float step =  speed * Time.deltaTime;
        if(transform.position.z - target.z >= 0.5f && touchTower)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, step);
        }
        if(transform.position.z - target.z <= 0.5f && touchTower)
        {
            CheckPath();
        }
        else
        {
            Debug.Log("no Tower now");
            transform.position = Vector3.MoveTowards(transform.position, defenderBase.transform.position, step);
        }
    }
    void CheckPath()
    {
        RaycastHit hit;
        Debug.DrawLine(transform.position, defenderBase.transform.position, Color.black);
        Physics.Raycast(transform.position, defenderBase.transform.position, out hit, Mathf.Infinity, Tower);
        //Debug.Log("Hit Tower Position: " + hit.transform.position);
        touchTower = Physics.Linecast(transform.position, defenderBase.transform.position, Tower);
        if(touchTower)
        {
            Debug.Log("hit Position is: " + hit.transform.position);
            target = new Vector3(hit.transform.position.x + 8, hit.transform.position.y, hit.transform.position.z);
        }

    }
    void MoveOffset()
    {
    }
}
