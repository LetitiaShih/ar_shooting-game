﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour {

	public GameObject tower;
	// Queue <Tower> TowersQueue=new Queue <Tower>();
	Queue <GameObject> TowersQueue = new Queue <GameObject>();
	public int towerLimit = 5;

	public void AddTower(Pose pose)
	{
		Debug.Log("*******Towers number*******" + TowersQueue.Count);
		int numTowers = TowersQueue.Count;
		if(numTowers < towerLimit)
		{
			InstantiateNewTower(pose);
		}
		else
		{
			RemoveTower();
		}
	}
	public void InstantiateNewTower(Pose pose)
	{
		Debug.Log("*******Add Towers*******");
		// Tower newTower  = Instantiate(tower, pose.position, Quaternion.identity);
		GameObject newTower  = Instantiate(tower, pose.position, Quaternion.identity);
		//newTower.transform.parent = towerParentTransform;

		TowersQueue.Enqueue(newTower);

	}
	void RemoveTower()
	{
		Debug.Log("*******Remove Towers*******");
		GameObject oldTower = TowersQueue.Dequeue();
		Debug.Log("Queue item number = " + TowersQueue.Count + ", dequeing " + oldTower);
		DestroyImmediate(oldTower);
	}

}
