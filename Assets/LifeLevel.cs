﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeLevel : MonoBehaviour {

	public DefenderHealth playerHealth;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float shiftAmount = -20f + (playerHealth.GetRatioOfLife()) * 20f;
		this.transform.localPosition=new Vector3(shiftAmount,0f,0f);
	}
}
