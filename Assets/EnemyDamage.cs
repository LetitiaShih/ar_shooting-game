﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{

    public Collider colliderMesh;
    public ParticleSystem hitEffect;
    public ParticleSystem deathEffect;
    public int damageEnemy = 1;
    public AudioClip enemyHitSFX;
    public AudioClip enemyDeathSFX;
    AudioSource myAudioSource;

    public int enemyLife = 10;
    void Start()
    {
        myAudioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void OnParticleCollision(GameObject other)
    {
        ProcessHit();
        if (enemyLife <= 0)
        {
            KillEnemy();
        }
    }
    void ProcessHit()
    {
        enemyLife -= damageEnemy;
        hitEffect.Play();
        //myAudioSource.PlayOneShot(enemyHitSFX);
    }
    void KillEnemy()
    {
        var deathEffectSpawnPos = new Vector3(transform.position.x, 7, transform.position.z);
        var deathEffectIns = Instantiate(deathEffect, deathEffectSpawnPos, Quaternion.identity);
        deathEffectIns.Play();
        // AudioSource.PlayClipAtPoint(enemyDeathSFX, Camera.main.transform.position);
        Destroy(deathEffectIns.gameObject, deathEffectIns.main.duration);
        Destroy(gameObject);
    }
}
